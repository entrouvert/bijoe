import json
import os

for root, dirs, files in os.walk('tests/fixtures/'):
    for name in files:
        if not name.endswith('.json'):
            continue
        path = os.path.join(root, name)
        with open(path) as fd:
            try:
                d = json.load(fd)
            except Exception as e:
                print('error on', path, ':', e)
                continue
        with open(path, 'w') as fd:
            json.dump(d, fd, indent=4, sort_keys=True, separators=(',', ': '))
