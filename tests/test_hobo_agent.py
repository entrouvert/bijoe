# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from bijoe.hobo_agent.management.commands import hobo_deploy


def test_schema_from_url():
    for hash_length in [4, 5, 6, 7]:
        for length in [64, 65, 66]:
            assert (
                len(hobo_deploy.schema_from_url('https://' + ('x' * length), hash_length=hash_length)) == 63
            )

    schema = hobo_deploy.schema_from_url(
        'https://demarches-saint-didier-au-mont-dor.guichet-recette.grandlyon.com/'
    )

    assert len(schema) == 63
    assert schema == 'demarches_saint_didier_au_mo0757cfguichet_recette_grandlyon_com'
