DROP SCHEMA IF EXISTS schema1 cascade;

CREATE SCHEMA schema1;

SET search_path = schema1;

CREATE TABLE category (
	id serial primary key,
	ord integer default(0) not null,
	label varchar not null);

CREATE TABLE subcategory (
	id serial primary key,
	category_id integer not null references schema1.category(id),
	ord integer default(0) not null,
	label varchar not null);

CREATE TABLE "Facts" (
    id serial primary key,
    date date,
    datetime timestamp with time zone,
    integer integer,
    boolean boolean,
    cnt integer default(1),
    innersubcategory_id integer references schema1.subcategory(id),
    leftsubcategory_id integer references schema1.subcategory(id),
    rightsubcategory_id integer references schema1.subcategory(id),
    outersubcategory_id integer references schema1.subcategory(id),
    "String" varchar,
    geo point,
    json jsonb
);

INSERT INTO category (ord, label) VALUES
   (1, 'caté1'),
   (0, 'caté2'),
   (0, 'caté3');

INSERT INTO subcategory (category_id, ord, label) VALUES
   (1, 1, 'subé1'),
   (1, 0, 'subé2'),
   (1, 0, 'subé3'),
   (2, 0, 'subé4'),
   (2, 0, 'subé5'),
   (2, 0, 'subé6'),
   (3, 1, 'subé7'),
   (3, 0, 'subé8'),
   (3, 0, 'subé9'),
   (3, 0, 'subé10'),
   (3, 0, 'subé11');


INSERT INTO "Facts" (date, datetime, integer, boolean, cnt, innersubcategory_id, leftsubcategory_id, rightsubcategory_id, outersubcategory_id, "String", geo, json) VALUES
   ('2017-01-01', '2017-01-01 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "x"}'),
   ('2017-01-02', '2017-01-02 10:00', 1, TRUE, 10, 3, 3, 3, 3, 'b', '(1, 1)', '{"a": "x"}'),
   ('2017-01-03', '2017-01-03 10:00', 1, FALSE, 10, NULL, NULL, NULL, NULL, 'a', '(1, 1)', '{"a": "x"}'),
   ('2017-01-04', '2017-01-04 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "x"}'),
   ('2017-01-05', '2017-01-05 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'c', '(1, 1)', '{"a": "x"}'),
   ('2017-01-06', '2017-01-06 10:00', 1, FALSE, 10, 1, 1, 1, 1, NULL, '(1, 1)', '{"a": "x"}'),
   ('2017-01-07', '2017-01-07 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "x"}'),
   ('2017-01-08', '2017-01-08 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-01-09', '2017-01-09 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-01-10', '2017-01-10 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-02-01', '2017-02-01 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-03-01', '2017-03-01 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'c', '(1, 1)', '{"a": "y"}'),
   ('2017-04-01', '2017-04-01 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-05-01', '2017-05-01 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-06-01', '2017-06-01 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'c', '(1, 1)', '{"a": "y"}'),
   ('2017-07-01', '2017-07-01 10:00', 1, FALSE, 10, 1, 1, 1, 1, 'a', '(1, 1)', '{"a": "y"}'),
   ('2017-08-01', '2017-08-01 10:00', 1, TRUE, 10, 1, 1, 1, 1, 'b', '(1, 1)', '{"a": "z"}');
