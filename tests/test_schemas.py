# bijoe - BI dashboard
# Copyright (C) 2019  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest
from django.utils.translation import gettext as _

from bijoe.schemas import Dimension


def test_absent_label():
    assert Dimension.from_json({'name': 'x', 'value': 'x', 'type': 'integer'}).absent_label == _('None')
    assert Dimension.from_json({'name': 'x', 'value': 'x', 'type': 'string'}).absent_label == _('None')
    assert Dimension.from_json({'name': 'x', 'value': 'x', 'type': 'bool'}).absent_label == _('N/A')
    assert (
        Dimension.from_json(
            {'name': 'x', 'value': 'x', 'type': 'boolean', 'absent_label': 'coin'}
        ).absent_label
        == 'coin'
    )

    with pytest.raises(NotImplementedError):
        Dimension.from_json({'name': 'x', 'value': 'x', 'type': 'coin'}).absent_label
