from datetime import date

from bijoe.relative_time import RelativeDate


def test_relative_date():
    today = date(2016, 3, 3)
    assert RelativeDate('cette année', today=today) == date(2016, 1, 1)
    assert RelativeDate('ce mois', today=today) == date(2016, 3, 1)
    assert RelativeDate('le mois dernier', today=today) == date(2016, 2, 1)
    assert RelativeDate('les 4 derniers mois', today=today) == date(2015, 11, 1)
    assert RelativeDate('le mois  prochain', today=today) == date(2016, 4, 1)
    assert RelativeDate('les  3  prochains mois', today=today) == date(2016, 6, 1)
    assert RelativeDate(' cette  semaine', today=today) == date(2016, 2, 29)
    assert RelativeDate(' maintenant', today=today) == today
    assert RelativeDate('2016-01-01', today=today) == date(2016, 1, 1)
