import json
import os
import re

import pytest
from tabulate import tabulate
from utils import get_table, login


def pytest_generate_tests(metafunc):
    if hasattr(metafunc, 'function'):
        fcode = metafunc.function.__code__
        if 'visualization' in fcode.co_varnames[: fcode.co_argcount]:
            with open(os.path.join(os.path.dirname(__file__), 'fixtures', 'schema2', 'tables.json')) as fd:
                tables = json.load(fd)
            metafunc.parametrize(['visualization'], [[x] for x in tables])


def assert_equal_tables(table1, table2):
    t1 = tabulate(table1)
    t2 = tabulate(table2)
    t1 = re.sub(' +', ' ', t1)
    t2 = re.sub(' +', ' ', t2)
    assert t1 == t2


@pytest.fixture(autouse=True)
def freezetime(freezer):
    freezer.move_to('2018-12-07 16:53:00')


def test_simple(request, schema2, app, admin, visualization):
    login(app, admin)
    response = app.get('/')

    visualization_page = response.click(lambda x: x == visualization)
    assert 'big-msg-info' not in visualization_page
    table = get_table(visualization_page)
    if request.config.getoption('--bijoe-store-table'):
        d = {}
        new_table_path = 'tests/fixtures/schema2/new_tables.json'
        if os.path.exists(new_table_path):
            with open(new_table_path) as fd:
                d = json.load(fd)
            if 'tables' in d:
                d = d['tables']
        d[visualization] = table
        with open(new_table_path, 'w') as fd:
            json.dump(d, fd, indent=4, sort_keys=True, separators=(',', ': '))
    assert_equal_tables(schema2['tables'][visualization], table)
