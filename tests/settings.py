BIJOE_CACHE = False
BIJOE_INIT_SQL = [
    'SET lc_time = \'fr_FR.UTF-8\'',
]
PAGE_LENGTH = 0
SELECT2_CACHE_PREFIX = 'select2_'

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]
