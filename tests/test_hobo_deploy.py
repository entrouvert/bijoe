# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import configparser
from contextlib import contextmanager

import pytest
import sentry_sdk
from psycopg2.extensions import parse_dsn

from bijoe.hobo_agent.management.commands import hobo_deploy


@contextmanager
def donothing(tenant):
    yield


class FakeTenant:
    domain_url = 'fake.tenant.com'

    def __init__(self, directory):
        self.directory = directory

    def get_directory(self):
        return self.directory


@pytest.fixture
def sentry():
    sentry_sdk.init(dsn='https://1234@sentry.example.com/1', environment='prod')
    yield
    sentry_sdk.init()


@pytest.mark.parametrize('with_sentry', [[True], [False]])
def test_deploy_specifics(tmpdir, settings, monkeypatch, request, with_sentry):
    monkeypatch.setattr(hobo_deploy, 'tenant_context', donothing)

    if with_sentry:
        request.getfixturevalue('sentry')

    settings.DATABASES = {
        'default': {
            'NAME': 'coucou',
            'HOST': 'hostname.zob.org',
            'USER': 'hep',
            'PASSWORD': 'a \'%fc',
            'PORT': 1234,
        }
    }
    hobo_environment = {
        'services': [
            {
                'this': True,
                'secret_key': 'xx',
            }
        ],
    }

    command = hobo_deploy.Command()
    tenant_dir = tmpdir.mkdir('tenant')
    tenant = FakeTenant(str(tenant_dir))

    command.deploy_specifics(hobo_environment, tenant)

    wcs_olap_ini_path = tenant_dir / 'wcs-olap.ini'
    assert wcs_olap_ini_path.exists()
    with wcs_olap_ini_path.open() as fd:
        config = configparser.ConfigParser()
        config.readfp(fd)
        pg_dsn = config.get('wcs-olap', 'pg_dsn')
        parsed_pg_dsn = parse_dsn(pg_dsn)
        assert parsed_pg_dsn['dbname'] == 'coucou'
        assert parsed_pg_dsn['host'] == 'hostname.zob.org'
        assert parsed_pg_dsn['user'] == 'hep'
        assert parsed_pg_dsn['password'] == 'a \'%fc'
        assert parsed_pg_dsn['port'] == '1234'

    bijoe_base_url = 'https://bijoe.example.net'
    wcs_base_url = 'https://wcs.example.net'
    hobo_environment = {
        'services': [
            {
                'this': True,
                'secret_key': 'xx',
                'service-id': 'bijoe',
                'base_url': bijoe_base_url,
            },
            {
                'this': False,
                'secret_key': 'yy',
                'service-id': 'wcs',
                'base_url': wcs_base_url,
                'title': 'my läâàlel',
                'slug': 'my_slug',
            },
            {
                'this': False,
                'secret_key': 'zz',
                'service-id': 'wcs',
                'base_url': 'https://wcs-2.example.net',
                'secondary': True,
            },
        ],
    }
    command.deploy_specifics(hobo_environment, tenant)
    with wcs_olap_ini_path.open() as fd:
        config = configparser.ConfigParser()
        config.readfp(fd)
        assert config.get(wcs_base_url, 'orig') == 'bijoe.example.net'
        assert config.get(wcs_base_url, 'schema') == 'wcs_example_net'
        assert config.get(wcs_base_url, 'cubes_label') == 'my läâàlel'
        assert config.get(wcs_base_url, 'cubes_slug') == 'my_slug'
        if with_sentry:
            assert config.get('sentry', 'dsn') == 'https://1234@sentry.example.com/1'
            assert config.get('sentry', 'environment') == 'prod'
        else:
            assert not config.has_section('sentry')

    hobo_environment['services'][0]['base_url'] = 'https://new-bijoe.example.net'
    command.deploy_specifics(hobo_environment, tenant)
    with wcs_olap_ini_path.open() as fd:
        config = configparser.ConfigParser()
        config.readfp(fd)
        assert config.get(wcs_base_url, 'orig') == 'new-bijoe.example.net'
