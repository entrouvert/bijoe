# bijoe - BI dashboard
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# from https://git.entrouvert.org/hobo.git/tree/tests/test_signature.py
import datetime
import urllib.parse

from bijoe.visualization import signature


def test_signature():
    KEY = 'xyz'
    STRING = 'aaa'
    URL = 'http://example.net/api/?coucou=zob'
    QUERY = 'coucou=zob'
    OTHER_KEY = 'abc'

    # Passing test
    assert signature.check_string(STRING, signature.sign_string(STRING, KEY), KEY)
    assert signature.check_query(signature.sign_query(QUERY, KEY), KEY)
    assert signature.check_url(signature.sign_url(URL, KEY), KEY)

    # Not passing tests
    assert not signature.check_string(STRING, signature.sign_string(STRING, KEY), OTHER_KEY)
    assert not signature.check_query(signature.sign_query(QUERY, KEY), OTHER_KEY)
    assert not signature.check_url(signature.sign_url(URL, KEY), OTHER_KEY)
    # assert not signature.check_url('%s&foo=bar' % signature.sign_url(URL, KEY), KEY)

    # Test URL is preserved
    assert URL in signature.sign_url(URL, KEY)
    assert QUERY in signature.sign_query(QUERY, KEY)

    # Test signed URL expected parameters
    assert '&algo=sha256&' in signature.sign_url(URL, KEY)
    assert '&timestamp=' in signature.sign_url(URL, KEY)
    assert '&nonce=' in signature.sign_url(URL, KEY)

    # Test unicode key conversion to UTF-8
    assert signature.check_url(signature.sign_url(URL, '\xe9\xe9'), b'\xc3\xa9\xc3\xa9')
    assert signature.check_url(signature.sign_url(URL, b'\xc3\xa9\xc3\xa9'), '\xe9\xe9')

    # Test timedelta parameter
    now = datetime.datetime.utcnow()
    assert '&timestamp=%s' % urllib.parse.quote(now.strftime('%Y-%m-%dT%H:%M:%SZ')) in signature.sign_url(
        URL, KEY, timestamp=now
    )

    # Test nonce parameter
    assert '&nonce=uuu&' in signature.sign_url(URL, KEY, nonce='uuu')

    # Test known_nonce
    assert signature.check_url(signature.sign_url(URL, KEY), KEY, known_nonce=lambda nonce: nonce == 'xxx')
    assert signature.check_url(
        signature.sign_url(URL, KEY, nonce='xxx'), KEY, known_nonce=lambda nonce: nonce == 'xxx'
    )

    # Test timedelta
    now = datetime.datetime.utcnow() - datetime.timedelta(seconds=29)
    assert signature.check_url(signature.sign_url(URL, KEY, timestamp=now), KEY)
    now = datetime.datetime.utcnow() - datetime.timedelta(seconds=30)
    assert not signature.check_url(signature.sign_url(URL, KEY, timestamp=now), KEY)
    now = datetime.datetime.utcnow() - datetime.timedelta(seconds=2)
    assert signature.check_url(signature.sign_url(URL, KEY, timestamp=now), KEY)
    assert signature.check_url(signature.sign_url(URL, KEY, timestamp=now), KEY, timedelta=2)
