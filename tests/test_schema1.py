import json

from utils import get_ods_document, get_ods_table, get_table, login, request_select2

from bijoe.visualization.models import Visualization as VisualizationModel
from bijoe.visualization.ods import OFFICE_NS, TABLE_NS
from bijoe.visualization.utils import Visualization


def test_simple(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    assert 'big-msg-info' in response
    assert 'le champ « pouët »' in response
    assert 'warning2' in response
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'innersubcategory')
    response = form.submit('visualize')
    assert 'big-msg-info' not in response
    assert get_table(response) == [
        [
            'Inner SubCategory',
            'sub\xe910',
            'sub\xe911',
            'sub\xe94',
            'sub\xe95',
            'sub\xe96',
            'sub\xe98',
            'sub\xe99',
            'sub\xe97',
            'sub\xe92',
            'sub\xe93',
            'sub\xe91',
        ],
        ['number of rows', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '15'],
    ]
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'date__month')
    response = form.submit('visualize')
    assert 'big-msg-info' not in response
    assert get_table(response) == [
        ['mois (Date)', 'janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt'],
        ['number of rows', '10', '1', '1', '1', '1', '1', '1', '1'],
    ]


def test_truncated_previous_year_range(schema1, app, admin, freezer):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'date__month')
    form.set('drilldown_y', 'date__year')
    form.set('filter__date_2', 'since_1jan_last_year')
    freezer.move_to('2019-01-01 01:00:00')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['', 'janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt', 'Total'],
        ['2017', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ]

    freezer.move_to('2018-01-01 01:00:00')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['', 'janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt', 'Total'],
        ['2017', '10', '1', '1', '1', '1', '1', '1', '1', '17'],
    ]


def test_boolean_dimension(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'boolean')
    response = form.submit('visualize')
    assert get_table(response) == [['Boolean', 'Oui', 'Non'], ['number of rows', '8', '9']]
    form['filter__boolean'].force_value(
        [o[0] for o in form.fields['filter__boolean'][0].options if o[2] == 'Oui'][0]
    )
    response = form.submit('visualize')
    assert get_table(response) == [['Boolean', 'Oui', 'Non'], ['number of rows', '8', '0']]


def test_string_dimension(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'string')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['String', 'a', 'b', 'c', 'Aucun(e)'],
        ['number of rows', '11', '2', '3', '1'],
    ]
    form['filter__string'].force_value(['a', 'b', '__none__'])
    response = form.submit('visualize')
    assert get_table(response) == [['String', 'a', 'b', 'Aucun(e)'], ['number of rows', '11', '2', '1']]


def test_string_dimension_json_data(schema1, app, admin):
    # test conversion to Javascript declaration
    visu = Visualization.from_json(
        {
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'table',
            'measure': 'simple_count',
            'drilldown_x': 'string',
        }
    )
    assert json.loads(json.dumps(visu.json_data())) == [
        {'coords': [{'value': 'a'}], 'measures': [{'value': 11}]},
        {'coords': [{'value': 'b'}], 'measures': [{'value': 2}]},
        {'coords': [{'value': 'c'}], 'measures': [{'value': 3}]},
        {'coords': [{'value': 'Aucun(e)'}], 'measures': [{'value': 1}]},
    ]


def test_item_dimension(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'outersubcategory')
    response = form.submit('visualize')
    assert get_table(response) == [
        [
            'Outer SubCategory',
            'sub\xe910',
            'sub\xe911',
            'sub\xe94',
            'sub\xe95',
            'sub\xe96',
            'sub\xe98',
            'sub\xe99',
            'sub\xe97',
            'sub\xe92',
            'sub\xe93',
            'sub\xe91',
            'Aucun(e)',
        ],
        ['number of rows', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '15', '1'],
    ]
    form['filter__outersubcategory'].force_value(['__none__'])
    response = form.submit('visualize')
    assert get_table(response) == [['Outer SubCategory', 'Aucun(e)'], ['number of rows', '1']]


def test_yearmonth_drilldown(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'date__yearmonth')
    response = form.submit('visualize')
    assert get_table(response) == [
        [
            'ann\xe9e et mois (Date)',
            '01/2017',
            '02/2017',
            '03/2017',
            '04/2017',
            '05/2017',
            '06/2017',
            '07/2017',
            '08/2017',
        ],
        ['number of rows', '10', '1', '1', '1', '1', '1', '1', '1'],
    ]


def test_ods(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'innersubcategory')
    form.set('drilldown_y', 'hour')
    response = form.submit('visualize')
    assert 'big-msg-info' not in response
    response = response.click(href='save')
    response.form['name'] = 'test'
    response = response.form.submit().follow()
    ods_response = response.click(href='ods')
    ods_url = ods_response.request.url
    # skip first line of ODS table as it's a header not present in the HTML display
    assert get_table(response) == get_ods_table(ods_response)[1:]
    root = get_ods_document(ods_response)
    nodes = root.findall('.//{%s}table-cell' % TABLE_NS)
    assert len([node for node in nodes if node.attrib['{%s}value-type' % OFFICE_NS] == 'float']) == 13

    app.reset()  # logout
    assert 'login' in app.get(ods_response.request.url, status=302).location


def test_truncated_previous_year_range_on_datetime(schema1, app, admin, freezer):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'date__month')
    form.set('drilldown_y', 'date__year')
    form.set('filter__datetime_2', 'since_1jan_last_year')
    freezer.move_to('2019-01-01 01:00:00')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['', 'janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt', 'Total'],
        ['2017', '0', '0', '0', '0', '0', '0', '0', '0', '0'],
    ]
    freezer.move_to('2018-01-01 01:00:00')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['', 'janvier', 'f\xe9vrier', 'mars', 'avril', 'mai', 'juin', 'juillet', 'ao\xfbt', 'Total'],
        ['2017', '10', '1', '1', '1', '1', '1', '1', '1', '17'],
    ]


def test_none_percent_json_data_0d(schema1, app, admin):
    # test conversion to Javascript declaration
    visu = Visualization.from_json(
        {
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'graphical',
            'measure': 'percent',
        }
    )
    assert visu.json_data() == [{'coords': [], 'measures': [{'value': 100.0}]}]


def test_none_percent_json_data_2d(schema1, app, admin):
    # test conversion to Javascript declaration
    visu = Visualization.from_json(
        {
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'graphical',
            'measure': 'percent',
            'drilldown_y': 'leftcategory',
            'drilldown_x': 'date__year',
        }
    )
    assert visu.json_data() == [
        {'coords': [{'value': '2017'}, {'value': 'cat\xe92'}], 'measures': [{'value': 0}]},
        {'coords': [{'value': '2017'}, {'value': 'cat\xe93'}], 'measures': [{'value': 0}]},
        {'coords': [{'value': '2017'}, {'value': 'cat\xe91'}], 'measures': [{'value': 94.11764705882354}]},
        {'coords': [{'value': '2017'}, {'value': 'Aucun(e)'}], 'measures': [{'value': 5.882352941176471}]},
    ]


def test_geoloc(schema1, app, admin):
    # test conversion to Javascript declaration
    visu = VisualizationModel.objects.create(
        slug='visu',
        name='Visu',
        parameters={
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'graphical',
            'measure': 'percent',
            'drilldown_y': 'outercategory',
            'drilldown_x': 'date__year',
        },
    )
    response = app.get('/visualization/%d/geojson/' % visu.pk)
    assert response.json == {
        'type': 'FeatureCollection',
        'features': [
            {
                'geometry': {
                    'coordinates': [
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                        [1.0, 1.0],
                    ],
                    'type': 'MultiPoint',
                },
                'properties': {'Outer Category': 'cat\xe91', 'ann\xe9e (Date)': '2017'},
                'type': 'Feature',
            },
            {
                'geometry': {'coordinates': [[1.0, 1.0]], 'type': 'MultiPoint'},
                'properties': {'Outer Category': 'Aucun(e)', 'ann\xe9e (Date)': '2017'},
                'type': 'Feature',
            },
            {
                'geometry': {'coordinates': [], 'type': 'MultiPoint'},
                'properties': {'Outer Category': 'cat\xe92', 'ann\xe9e (Date)': 'Aucun(e)'},
                'type': 'Feature',
            },
            {
                'geometry': {'coordinates': [], 'type': 'MultiPoint'},
                'properties': {'Outer Category': 'cat\xe93', 'ann\xe9e (Date)': 'Aucun(e)'},
                'type': 'Feature',
            },
            {
                'geometry': {'coordinates': [], 'type': 'MultiPoint'},
                'properties': {'Outer Category': 'cat\xe91', 'ann\xe9e (Date)': 'Aucun(e)'},
                'type': 'Feature',
            },
        ],
    }


def test_filter_type_mismatch(schema1, app, admin):
    # test conversion to Javascript declaration
    visu = Visualization.from_json(
        {
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'graphical',
            'measure': 'simple_count',
            'filters': {
                'string': [1],
            },
        }
    )
    assert visu.json_data() == [{'coords': [], 'measures': [{'value': 0}]}]


def test_empty_filter(schema1, app, admin):
    visu = Visualization.from_json(
        {
            'warehouse': 'schema1',
            'cube': 'facts1',
            'representation': 'graphical',
            'measure': 'simple_count',
            'filters': {
                'innercategory': [],
            },
        }
    )
    assert visu.json_data() == [{'coords': [], 'measures': [{'value': 17}]}]


def test_json_dimensions(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form

    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_x', 'a')
    response = form.submit('visualize')
    assert get_table(response) == [['A', 'x', 'y', 'z'], ['number of rows', '7', '9', '1']]

    assert 'filter__a' in form.fields
    choices = [o['id'] for o in request_select2(app, response, 'filter__a')['results']]
    assert set(choices) == {'x', 'y', 'z', '__none__'}

    form['filter__a'].force_value(['x', 'y'])
    response = form.submit('visualize')
    assert get_table(response) == [['A', 'x', 'y', 'z'], ['number of rows', '7', '9', '0']]


def test_json_dimensions_having_percent(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form

    form.set('representation', 'table')
    form.set('measure', 'percent')
    form.set('drilldown_x', 'a')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['A', 'x', 'y', 'z'],
        ['pourcentage des demandes', '41,18 %', '52,94 %', '5,88 %'],
    ]

    assert 'filter__a' in form.fields
    choices = [o['id'] for o in request_select2(app, response, 'filter__a')['results']]
    assert set(choices) == {'x', 'y', 'z', '__none__'}

    form['filter__a'].force_value(['x', 'y'])
    response = form.submit('visualize')
    assert get_table(response) == [
        ['A', 'x', 'y', 'z'],
        ['pourcentage des demandes', '43,75 %', '56,25 %', '0,00 %'],
    ]


def test_sum_integer_measure(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'sum_integer')
    form.set('drilldown_x', 'string')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['String', 'a', 'b', 'c', 'Aucun(e)'],
        ['sum of integer column', '11', '2', '3', '1'],
    ]
    form['filter__string'].force_value(['a', 'b', '__none__'])
    response = form.submit('visualize')
    assert get_table(response) == [
        ['String', 'a', 'b', 'Aucun(e)'],
        ['sum of integer column', '11', '2', '1'],
    ]


def test_select2_filter_widget(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')

    resp = request_select2(app, response, 'filter__innersubcategory')
    assert len(resp['results']) == 10
    assert resp['more'] is True

    resp = request_select2(app, response, 'filter__innersubcategory', page=2)
    assert len(resp['results']) == 2
    assert resp['more'] is False

    resp = request_select2(app, response, 'filter__innersubcategory', term='aucun')
    assert len(resp['results']) == 1
    assert resp['more'] is False

    resp = request_select2(app, response, 'filter__innersubcategory', term='é')
    assert len(resp['results']) == 10
    assert resp['more'] is True

    resp = request_select2(app, response, 'filter__innersubcategory', term='é', page=2)
    assert len(resp['results']) == 1
    assert resp['more'] is False


def test_date_dimension(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('drilldown_y', 'date')
    form.set('filter__date_0', '2017-01-01')
    form.set('filter__date_1', '2017-02-01')
    response = form.submit('visualize')
    assert get_table(response) == [
        ['Date', 'number of rows'],
        ['01/01/2017', '1'],
        ['02/01/2017', '1'],
        ['03/01/2017', '1'],
        ['04/01/2017', '1'],
        ['05/01/2017', '1'],
        ['06/01/2017', '1'],
        ['07/01/2017', '1'],
        ['08/01/2017', '1'],
        ['09/01/2017', '1'],
        ['10/01/2017', '1'],
    ]


def test_date_dimension_loop(schema1, app, admin):
    login(app, admin)
    response = app.get('/')
    response = response.click('schema1')
    response = response.click('Facts 1')
    form = response.form
    form.set('representation', 'table')
    form.set('measure', 'simple_count')
    form.set('loop', 'date')
    form.set('filter__date_0', '2017-01-01')
    form.set('filter__date_1', '2017-02-01')
    response = form.submit('visualize')
    assert get_table(response) == [['number of rows', '1']] * 10
