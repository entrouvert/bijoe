import json
import os
import shutil
import sys
import tempfile
from io import StringIO

import pytest
from django.core.management import call_command
from django.utils.encoding import force_bytes

from bijoe.utils import import_site
from bijoe.visualization.models import Visualization

pytestmark = pytest.mark.django_db


def get_output_of_command(command, *args, **kwargs):
    old_stdout = sys.stdout
    output = sys.stdout = StringIO()
    call_command(command, *args, **kwargs)
    sys.stdout = old_stdout
    return output.getvalue()


def test_import_export(schema1, app):
    parameters = {
        'cube': 'facts1',
        'warehouse': 'schema1',
        'measure': 'duration',
        'representation': 'table',
        'loop': '',
        'filters': {},
        'drilldown_x': 'date__yearmonth',
    }

    def create_visu(i=0):
        Visualization.objects.create(name='test' + str(i), parameters=parameters)

    for i in range(3):
        create_visu(i)
    output = get_output_of_command('export_site')
    assert len(json.loads(output)['visualizations']) == 3

    import_site(data={}, clean=True)
    empty_output = get_output_of_command('export_site')
    assert len(json.loads(empty_output)['visualizations']) == 0

    create_visu()
    old_stdin = sys.stdin
    sys.stdin = StringIO(json.dumps({}))
    assert Visualization.objects.count() == 1
    try:
        call_command('import_site', '-', clean=True)
    finally:
        sys.stdin = old_stdin
    assert Visualization.objects.count() == 0

    with tempfile.NamedTemporaryFile() as f:
        f.write(force_bytes(output))
        f.flush()
        call_command('import_site', f.name)
    assert Visualization.objects.count() == 3
    for i in range(3):
        visu = Visualization.objects.get(name='test' + str(i))
        assert visu.parameters == parameters

    visu = Visualization.objects.get(name='test0')
    slug = visu.slug
    visu_json = visu.export_json()
    visu_json['name'] = 'new_name'
    visu_json['parameters']['measure'] = 'test'
    result = import_site(data={'visualizations': [visu_json]})
    assert result['created'] == 0 and result['updated'] == 1
    visu = Visualization.objects.get(slug=slug)
    assert visu.name == 'new_name'
    new_params = visu.parameters
    assert new_params.pop('measure') == 'test'
    assert new_params == {k: v for k, v in parameters.items() if k != 'measure'}

    import_site(data={}, if_empty=True)
    assert Visualization.objects.count() == 3

    import_site(data={}, clean=True)
    tempdir = tempfile.mkdtemp('bijoe-test')
    empty_output = get_output_of_command('export_site', output=os.path.join(tempdir, 't.json'))
    assert os.path.exists(os.path.join(tempdir, 't.json'))
    shutil.rmtree(tempdir)
