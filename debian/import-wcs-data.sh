#!/bin/bash

LOG=`tempfile`

trap "rm $LOG" EXIT

for tenant in /var/lib/bijoe/tenants/*; do
    if [ -n "${tenant##*.invalid*}" -a -f $tenant/wcs-olap.ini ]; then
        wcs-olap --all $tenant/wcs-olap.ini >$LOG 2>&1 || cat $LOG
    fi
done
