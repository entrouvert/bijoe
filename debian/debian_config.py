# This file is sourced by "exec" from bijoe.settings

import os

PROJECT_NAME = 'bijoe'

#
# SAML-isation
#
INSTALLED_APPS = INSTALLED_APPS + ('mellon',)

exec(open('/usr/lib/hobo/debian_config_common.py').read())

# SAML2 authentication
AUTHENTICATION_BACKENDS = ('mellon.backends.SAMLBackend',)
MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []

# Override default hobo agent
INSTALLED_APPS = ('bijoe.hobo_agent',) + INSTALLED_APPS

WCS_OLAP_COMMAND = '/usr/bin/wcs-olap'


#
# local settings
#
exec(open(os.path.join(ETC_DIR, 'settings.py')).read())

# run additional settings snippets
exec(open('/usr/lib/hobo/debian_config_settings_d.py').read())
