"""
In order to allow for import/export between instances, warehouses are now
retrieved by slug and not by name.
This new information is available in the model files, but for existing
visualizations it should be manually added, using this script.
For a standard multitenants install, it can be ran using the command :
sudo -u bijoe bijoe-manage tenant_command runscript --all-tenants /usr/share/doc/bijoe/warehouse_slug.py
"""

from bijoe.utils import get_warehouses
from bijoe.visualization.models import Visualization

warehouses = get_warehouses()
for visu in Visualization.objects.all():
    for warehouse in warehouses:
        if warehouse.name == visu.parameters['warehouse']:
            visu.parameters['warehouse_slug'] = warehouse.slug
            visu.save()
            break
