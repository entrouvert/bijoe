# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import resolve_url
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.http import quote
from django.utils.translation import gettext as _
from django.views.decorators.cache import never_cache
from django.views.generic import ListView, View

try:
    from mellon.utils import get_idps
except ImportError:
    get_idps = lambda: []

from .engine import Engine
from .utils import get_warehouses
from .visualization.models import Visualization
from .visualization.utils import Visualization as VisuUtil


class AuthorizationMixin:
    def authorize(self, request):
        if request.user.is_authenticated:
            if not request.user.is_superuser:
                raise PermissionDenied(_('You must be superuser'))
            return True
        else:
            return False

    def dispatch(self, request, *args, **kwargs):
        if self.authorize(request):
            return super().dispatch(request, *args, **kwargs)
        else:
            return redirect_to_login(request.build_absolute_uri())


class HomepageView(AuthorizationMixin, ListView):
    template_name = 'bijoe/homepage.html'
    model = Visualization
    context_object_name = 'visualizations'
    paginate_by = settings.PAGE_LENGTH

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['warehouses'] = sorted((Engine(w) for w in get_warehouses()), key=lambda w: w.label)
        ctx['request'] = self.request
        return ctx

    def get_queryset(self):
        return self.model.all_visualizations()


class MenuJSONView(AuthorizationMixin, View):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/json')
        menu = [
            {
                'label': _('Statistics'),
                'slug': 'statistics',
                'url': request.build_absolute_uri(reverse('homepage')),
            }
        ]
        json_str = json.dumps(menu)
        for variable in ('jsonpCallback', 'callback'):
            if variable in request.GET:
                response = HttpResponse(content_type='application/javascript')
                json_str = '%s(%s);' % (request.GET[variable], json_str)
                break
        response.write(json_str)
        return response


homepage = HomepageView.as_view()
menu_json = MenuJSONView.as_view()


class LoginView(auth_views.LoginView):
    template_name = 'bijoe/login.html'

    def get(self, request, *args, **kwargs):
        if any(get_idps()):
            if 'next' not in request.GET:
                return HttpResponseRedirect(resolve_url('mellon_login'))
            return HttpResponseRedirect(
                resolve_url('mellon_login') + '?next=' + quote(request.GET.get('next'))
            )
        return super().get(request, *args, **kwargs)


login = LoginView.as_view()


class LogoutView(auth_views.LogoutView):
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if any(get_idps()):
            return HttpResponseRedirect(resolve_url('mellon_logout'))
        return super().dispatch(request, *args, **kwargs)


logout = LogoutView.as_view()
