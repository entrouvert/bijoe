from django.db import migrations


def rename_parameters(apps, schema_editor):
    Visualization = apps.get_model('visualization', 'Visualization')
    for v in Visualization.objects.all():
        v.parameters['drilldown_y'] = (v.parameters.get('drilldown') or [None])[0]
        v.parameters['measure'] = (v.parameters.get('measures') or [None])[0]
        v.parameters.pop('drilldown', None)
        v.parameters.pop('measures', None)
        v.save()


def reverse_rename_parameters(apps, schema_editor):
    Visualization = apps.get_model('visualization', 'Visualization')
    for v in Visualization.objects.all():
        drilldown_y = v.parameters.get('drilldown_y')
        v.parameters['drilldown'] = [drilldown_y] if drilldown_y else []
        measure = v.parameters.get('measure')
        v.parameters['measures'] = [measure] if measure else []
        v.parameters.pop('drilldown_y', None)
        v.parameters.pop('measure', None)
        v.save()


class Migration(migrations.Migration):

    dependencies = [
        ('visualization', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(rename_parameters, reverse_rename_parameters),
    ]
