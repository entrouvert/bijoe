from django.contrib.postgres.fields import JSONField
from django.db import migrations, models

import bijoe.visualization.models


class Migration(migrations.Migration):

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Visualization',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.TextField(verbose_name='name')),
                (
                    'parameters',
                    JSONField(
                        default=dict,
                        verbose_name='parameters',
                        encoder=bijoe.visualization.models.JSONEncoder,
                    ),
                ),
            ],
            options={
                'ordering': ('name', 'id'),
                'verbose_name': 'visualization',
                'verbose_name_plural': 'visualizations',
            },
        ),
    ]
