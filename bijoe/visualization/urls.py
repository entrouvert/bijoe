# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    path('', views.visualizations, name='visualizations'),
    path('json/', views.visualizations_json, name='visualizations-json'),
    path('import/', views.visualizations_import, name='visualizations-import'),
    path('export', views.visualizations_export, name='visualizations-export'),
    re_path(r'^warehouse/(?P<warehouse>[^/]*)/$', views.warehouse, name='warehouse'),
    re_path(r'^warehouse/(?P<warehouse>[^/]*)/(?P<cube>[^/]*)/$', views.cube, name='cube'),
    re_path(
        r'^warehouse/(?P<warehouse>[^/]*)/(?P<cube>[^/]*)/iframe/$', views.cube_iframe, name='cube-iframe'
    ),
    re_path(
        r'warehouse/(?P<warehouse>[^/]*)/(?P<cube>[^/]*)/save/$',
        views.create_visualization,
        name='create-visualization',
    ),
    path('<int:pk>/', views.visualization, name='visualization'),
    path('<int:pk>/json/', views.visualization_json, name='visualization-json'),
    path('<int:pk>/geojson/', views.visualization_geojson, name='visualization-geojson'),
    path('<int:pk>/iframe/', views.visualization_iframe, name='visualization-iframe'),
    path('<int:pk>/ods/', views.visualization_ods, name='visualization-ods'),
    path('<int:pk>/rename/', views.rename_visualization, name='rename-visualization'),
    path('<int:pk>/delete/', views.delete_visualization, name='delete-visualization'),
    path('<int:pk>/export', views.export_visualization, name='export-visualization'),
    path('<int:pk>/save-as/', views.save_as_visualization, name='save-as-visualization'),
    re_path(r'^select2choices.json$', views.select2_choices, name='select2-choices'),
]
