# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import json

from django.db import models
from django.db.models import JSONField
from django.http import Http404
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _

from bijoe.utils import get_warehouses

from .utils import Visualization as VisuUtils


class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, datetime.date):
            return obj.isoformat()
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


class Visualization(models.Model):
    slug = models.SlugField(verbose_name=_('Identifier'), unique=True, max_length=200)
    name = models.TextField(verbose_name=_('name'))
    parameters = JSONField(verbose_name=_('parameters'), default=dict, encoder=JSONEncoder)

    class Meta:
        ordering = ('name', 'id')
        verbose_name = _('visualization')
        verbose_name_plural = _('visualizations')

    def __str__(self):
        return self.name

    def natural_key(self):
        return (self.slug,)

    def export_json(self):
        visualization = {'slug': self.slug, 'name': self.name, 'parameters': self.parameters}
        return visualization

    @classmethod
    def import_json(cls, data):
        defaults = {'name': data['name'], 'parameters': data['parameters']}
        _, created = cls.objects.update_or_create(slug=data['slug'], defaults=defaults)
        return created

    def save(self, *args, **kwargs):
        if not self.slug:
            slug = base_slug = slugify(self.name)[:40].strip('-')
            i = 1
            while True:
                try:
                    Visualization.objects.get(slug=slug)
                except Visualization.DoesNotExist:
                    break
                i += 1
                slug = '%s-%s' % (base_slug, i)
            self.slug = slug
        return super().save(*args, **kwargs)

    @property
    def exists(self):
        try:
            VisuUtils.get_cube(self.parameters, self.warehouses)
            return True
        except Http404:
            return False

    @property
    def warehouses(self):
        if not hasattr(self, '_warehouses'):
            self._warehouses = get_warehouses()
        return self._warehouses

    @classmethod
    def all_visualizations(cls):
        visualizations = cls.objects.all()
        warehouses = get_warehouses()
        for visu in visualizations:
            visu._warehouses = warehouses
        return visualizations
