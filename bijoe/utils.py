# bijoe - BI dashboard
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import glob
import json
import os

from django.conf import settings
from django.db import connection, transaction
from django.utils.translation import gettext as _

try:
    from functools import lru_cache
except ImportError:
    from django.utils.lru_cache import lru_cache

from .schemas import Warehouse


def get_warehouses_paths():
    for pattern in settings.BIJOE_SCHEMAS:
        yield from glob.glob(pattern)
    if hasattr(connection, 'tenant'):
        pattern = os.path.join(connection.tenant.get_directory(), 'schemas', '*.model')
        yield from glob.glob(pattern)


@lru_cache()
def get_warehouse_by_path_and_mtime(path, mtime):
    with open(path) as fd:
        warehouse = json.load(fd)
    warehouse['path'] = path
    warehouse['timestamp'] = mtime
    return Warehouse.from_json(warehouse)


def get_warehouses_by_paths(paths):
    warehouses = []
    for path in paths:
        mtime = os.path.getmtime(path)
        warehouses.append(get_warehouse_by_path_and_mtime(path, mtime))
    return warehouses


def get_warehouses():
    paths = get_warehouses_paths()
    return get_warehouses_by_paths(paths)


def human_join(l):
    if not l:
        return ''
    if len(l) == 1:
        return l[0]
    if len(l) > 2:
        l = ', '.join(l[:-1]), l[-1]
    return _('{0} and {1}').format(l[0], l[1])


def export_site():
    from bijoe.visualization.models import Visualization

    return {'visualizations': [v.export_json() for v in Visualization.objects.all()]}


def import_site(data, if_empty=False, clean=False):
    from bijoe.visualization.models import Visualization

    if if_empty and Visualization.objects.exists():
        return

    if clean:
        Visualization.objects.all().delete()

    results = {'created': 0, 'updated': 0}
    with transaction.atomic():
        for data in data.get('visualizations', []):
            created = Visualization.import_json(data)
            if created:
                results['created'] += 1
            else:
                results['updated'] += 1
    return results
