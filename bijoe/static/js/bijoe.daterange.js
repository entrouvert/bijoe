function bijoe_date_range(id) {
    var $input1 = $(id + '_0');
    var $input2 = $(id + '_1');
    var $select = $(id + '_2');

    function update() {
        if ($select.val()) {
            $input1.prop('disabled', true);
            $input2.prop('disabled', true);
        } else {
            $input1.prop('disabled', false);
            $input2.prop('disabled', false);
        }
    };
    update();

    $select.on('change', update);
}
