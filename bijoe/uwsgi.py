# bijoe - BI dashboard
# Copyright (C) 2021  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import contextlib
import logging
import os
import subprocess

from django.conf import settings
from django.db import connection
from uwsgidecorators import cron, spool

# existing loggers are disabled by Django on django.setup() due do
# @'disable_existing_loggers': True@, this module must be loaded after
# django.setup() (or get_wsgi_application()) for the logger to work
logger = logging.getLogger(__name__)


@contextlib.contextmanager
def log_exception(name):
    try:
        yield
    except Exception:
        logger.exception('Exception during %s', name)


@spool(pass_arguments=True)
@log_exception('wcs-olap job')
def wcs_olap(wcs_olap_ini_paths):
    for wcs_olap_ini_path in wcs_olap_ini_paths:
        launch_wcs_olap(wcs_olap_ini_path)


@cron(0, 1, -1, -1, -1, target='spooler')
@log_exception('enqueuing of wcs-olap jobs')
def cron_enqueue_wcs_olap(num):
    if getattr(settings, 'DISABLE_CRON_JOBS'):
        return
    enqueue_wcs_olap()


def launch_wcs_olap(wcs_olap_ini_path):
    logger.info('start wcs-olap on %s', wcs_olap_ini_path)
    subprocess.run(
        [
            settings.WCS_OLAP_COMMAND,
            '--all',
            wcs_olap_ini_path,
        ],
        check=False,
    )
    logger.info('finished wcs-olap on %s', wcs_olap_ini_path)


def enqueue_wcs_olap():
    from hobo.multitenant.middleware import TenantMiddleware

    tenant_by_idp = {}
    for tenant in TenantMiddleware.get_tenants():
        wcs_olap_ini_path = os.path.join(tenant.get_directory(), 'wcs-olap.ini')
        if not os.path.exists(wcs_olap_ini_path):
            continue
        connection.set_tenant(tenant)
        if getattr(settings, 'TENANT_DISABLE_CRON_JOBS', False):
            continue
        try:
            idp_url = list(settings.KNOWN_SERVICES['authentic'].values())[0]['url']
        except (KeyError, IndexError):
            # add tenant with a unique value as idp
            tenant_by_idp[id(tenant)] = [wcs_olap_ini_path]
        else:
            if idp_url not in tenant_by_idp:
                tenant_by_idp[idp_url] = []
            tenant_by_idp[idp_url].append(wcs_olap_ini_path)

    for wcs_olap_ini_paths in tenant_by_idp.values():
        logger.info('enqueuing wcs-olap job for %s', wcs_olap_ini_paths)
        wcs_olap(wcs_olap_ini_paths)
