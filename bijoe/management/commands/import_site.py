# bijoe - BI dashboard
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys

from django.core.management.base import BaseCommand

from bijoe.utils import import_site


class Command(BaseCommand):
    help = 'Import an exported site'

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME', type=str, help='name of file to import')
        parser.add_argument('--clean', action='store_true', default=False, help='Clean site before importing')
        parser.add_argument(
            '--if-empty', action='store_true', default=False, help='Import only if site is empty'
        )

    def handle(self, filename, **options):
        if filename == '-':
            fd = sys.stdin
        else:
            fd = open(filename)
        import_site(json.load(fd), if_empty=options['if_empty'], clean=options['clean'])
