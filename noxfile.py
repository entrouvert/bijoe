import os
from pathlib import Path

import nox

nox.options.reuse_venv = True

HOBO_VERSION = os.getenv('HOBO_VERSION', 'main')


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version, drf_version):
    packages = [
        f'django{django_version}',
        'coverage',
        'pytest',
        'pytest-cov',
        'pytest-django',
        'pytest-freezer',
        'pytest-xdist',
        'WebTest',
        'django-webtest',
        'mock',
        'pyquery',
        'tabulate',
        'sentry_sdk<0.12.3',
        'setuptools',
        'psycopg2',
        'psycopg2-binary',
        'git+https://git.entrouvert.org/entrouvert/xstatic-chartnew-js',
        f'djangorestframework{drf_version}',
        f'https://git.entrouvert.org/entrouvert/hobo/archive/{HOBO_VERSION}.tar.gz',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize(
    'django_version,drf_version',
    [('>=4.2,<4.3', '>=3.9,<3.15')],
    ids=['django4'],
)
def tests(session, django_version, drf_version):
    setup_venv(session, django_version=django_version, drf_version=drf_version)

    session.run('python', 'manage.py', 'compilemessages', silent=True)

    args = [
        'pg_virtualenv',
        '-owal_level=minimal',
        '-omax_wal_senders=0',
        '-owal_log_hints=off',
        '-ocheckpoint_timeout=1d',
        '-osynchronous_commit=off',
        '-ofull_page_writes=off',
        '-ofsync=off',
        'py.test',
    ]

    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--junitxml=junit-django.xml',
            '--cov=bijoe',
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov-config',
            '.coveragerc',
            '--cov-context=test',
        ]

    args += session.posargs

    if not session.interactive:
        args += ['-v', '--numprocesses', '6']

    hookable_run(
        session,
        *args,
        env={
            'DJANGO_SETTINGS_MODULE': 'bijoe.settings',
            'BIJOE_SETTINGS_FILE': 'tests/settings.py',
            'SETUPTOOLS_USE_DISTUTILS': 'stdlib',
        },
        external=True,
    )


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
